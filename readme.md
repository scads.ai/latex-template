# Latex Template for ScaDS.AI Presentation and Poster

## About

This repository contains ScaDS.AI latex template of,

- [presentation](presentation/)
- [poster](poster/)
- [report](report/)

The current version of the templates can be downloaded from the [release page](https://gitlab.hrz.tu-chemnitz.de/scads.ai/latex-template/-/releases). The release download contains the necessary files to get you started. Feel free to also look at
   - [example presentation](other/sample_presentation.pdf)
   - [example poster](other/sample_poster.pdf)
   - [example report](other/sample_report.pdf)

## How to use the template

The template can be used using online, local or dockerized setup. Latest version of thepresentation and poster templates can be downloaded from the [release page](https://gitlab.hrz.tu-chemnitz.de/scads.ai/latex-template/-/releases). Once the templates are dowloaded and extracted, they can be used in following ways.

### 1. Online method with sharelatex

1. Archive the presentation or poster contents as a zip file. (From CLI, use `make presentation-zip` or `make poster-zip`.)
2. Go to [sharelatex](https://tex.zih.tu-dresden.de/), click on "new project".
3. Upload the zip file.
4. Change the following settings by clicking on top left sharelatex icon:
   - Compiler : XeLaTex
   - Main Document : Entrypoint document (here, main.tex)
5. Proceed with your project.

### 2. Local setup method

In this method all the required packages will be installed on the machine and the user will be able compile the files locally.

> This method requires installation of [make](https://www.gnu.org/software/make/). It can be installed on [Linux](https://askubuntu.com/a/753113) as well as on [Windows](https://stackoverflow.com/questions/32127524/how-to-install-and-use-make-in-windows) OS.

1. Copy the contents of the presentation or the poster to the desired location.
2. Copy the [Makefile](Makefile) to the location where the presentation/poster data is copied.
3. Change the values of following variables (or wherever it is necessary) inside the Makefile.
   - `TARGET` : Main (Entrypoint) latex filename without extention. Eg. Use `TARGET=main`, if main.tex is the entrypoint.
4. Launch the CLI and change to the directory where all the files in step 2 are copied.
5. Install the required packages

   ```bash
   make setup-local
   ```

6. Proceed with your project using different make commands.

   ```bash
    make build      # To build the project
    make watch      # To automatically track the changes and build the projects 
    make clean      # Clean all the files including compiled pdf
   ```

   Use `make help` for more information.

### 3. Local setup from command line

Assuming you have `make` installed (see above), you can create your project with the following steps:

1. Launch a terminal here.
2. Install the required packages using:

   ```bash
   make setup-local
   ```

3. Create a folder for your presentation or poster using `make` (replacing `/tmp/my-latex-project` as needed):

   ```bash
   make presentation AT_LOCATION=/tmp/my-latex-project
   ```

   or:

   ```bash
   make poster AT_LOCATION=/tmp/my-latex-project
   ```

4. Navigate to your LaTeX project:

   ```bash
   cd /tmp/my-latex-project
   ```

5. Proceed with your project using different make commands.

   ```bash
    make build      # To build the project
    make watch      # To automatically track the changes and build the projects 
    make clean      # Clean all the files including compiled pdf
   ```

   Use `make help` for more information.

### 4. Dockerized local setup

In this method user can use dockerized container to install the packages and build the latex projects. User do not have to install anything on its machine/workstation. The only requirement here is to have docker installed on the workstation. Information on how to install docker can be found [here](https://docs.docker.com/get-docker/).

> This method requires installation of [make](https://www.gnu.org/software/make/). It can be installed on [Linux](https://askubuntu.com/a/753113) as well as on [Windows](https://stackoverflow.com/questions/32127524/how-to-install-and-use-make-in-windows) OS.

Once the docker is installed, proceed as follows:

1. Copy the contents of the presentation or the poster to the desired location.
2. Copy the [Makefile](Makefile) and [Dockerfile](Dockerfile) to the location where the presentation/poster data is copied.
3. Change the values of following variables (or wherever it is necessary) inside the Makefile (the one you have copied in step 2).
   - `TARGET` : Main (Entrypoint) latex filename without extention. Eg. Use `TARGET=main`, if main.tex is the entrypoint.
   - `DOCKER_IMAGE_NAME` : Name of the docker image that will be used for all the latex projects. We build the image only once.
   - `DOCKER_CONTAINER_NAME` : Project specific container name to be created from the docker image. We build different containers from the image (above), for different projects.
4. Launch the CLI and change to the directory where all the files in step 2 are copied.
5. Create a docker image. It takes some time to build the image.

   ```bash
   make docker_build_image
   ```
6. Proceed with your project using different make commands for the docker setup.

   ```bash
    make dbuild
    make dwatch
    make clean
   ```

7. Run `make help` for more information.

### Installing new packages

1. Sharelatex: Packages are automatically installed
2. Local setup: Add package names in the [latex-packages-user.txt](latex-packages-user.txt), with each package in new line. Then run following:
   ```bash
   make install_latex_package
   ```
   Make sure the file `latex-packages-user.txt` is in the same directory as the `Makefile`.
3. Docker setup:
   Add package names in the [latex-packages-user.txt](latex-packages-user.txt), with each package in new line. Then rebuild the docker image from Dockerfile:
   ```bash
   make docker_build_image
   ```
   Make sure the file `latex-packages-user.txt` is in the same directory as the `Makefile`.

## Contributors

- Apurv Deepak Kulkarni
- Jan Frenzel
- Taras Lazariv
- Gina Valentin

Feel free to contribute to this project via the development repository [ScaDS.AI LaTeX Template Gitlab repository](https://gitlab.hrz.tu-chemnitz.de/scads.ai/latex-template/).
