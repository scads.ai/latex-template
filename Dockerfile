FROM texlive/texlive:latest-small

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get -y update; apt-get -y upgrade

RUN apt-get install -y make inotify-tools libfontconfig1 fontconfig

ENV PATH=$PATH:/usr/local/texlive/2024/bin/x86_64-linux

# Fixing repository
RUN tlmgr option repository http://mirror.ctan.org/systems/texlive/tlnet

RUN tlmgr update --self

# Installing required packages
RUN tlmgr install latexmk tudscr newunicodechar csquotes isodate blindtext acronym bigfoot xstring enumitem esvect multirow footmisc biblatex substr biber zref marginnote soulpos textpos wasysym adjustbox comment transparent pgfplots appendixnumberbeamer titlesec wrapfig printlen hyphenat collection-fontsrecommended pdfcomment datetime2 fontawesome5 lstaddons

# Installing additional latex packages provided by users
COPY latex-packages-user.txt /
RUN while read i; do tlmgr install $i; done < /latex-packages-user.txt

# Following required to resolve font cache issue
# Source: https://github.com/pandoc/dockerfiles/issues/180
ENV OSFONTDIR=/usr/share/fonts
RUN chmod -R o+w /usr/local/texlive/2024/texmf-var \
  && fc-cache --really-force --verbose

# Final update
RUN apt-get -y update; apt-get -y upgrade
RUN tlmgr update --self --all

# Cleanup
RUN apt-get purge -yq *-doc && \
    apt-get clean -yq && \
    rm -rf /var/lib/apt/lists/*
