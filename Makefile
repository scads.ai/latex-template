# Makefile to compile latex files
#------------------------------------------------------------------------------

MYSHELL="bash"

# Name of entrypoint latex file without extention. Eg for "main.tex", following 
# will be the target
TARGET=main

# Name of the docker image that will be used for all the latex projects
DOCKER_IMAGE_NAME=scadsai_latex_docker

# Project specific container name to be created from image docker image
DOCKER_CONTAINER_NAME=my_container_name

# Texlive installation directory for local setup
TEXLIVE_INSTALL_DIR=$$HOME/.texlive
TEXLIVE_INSTALL_MIRROR="http://mirror.ctan.org"

# Files to track the changes
TRACK_FILES=./*.tex ./*/*.tex

# ShareLatexURL
SHARELATEX_URL := https://tex.zih.tu-dresden.de/project

# Allowed keywords
ALLOWED_TYPES := poster presentation report
ALLOWED_DOCKER_CMD := watch build

PERCENT:=%

LATEXMK_OPTIONS=-quiet

# Compiler to be used
# use "--aux-directory=$(AUX_DIR)" insead of -output-directory, if MikTex Distribution is used instead TexLive
COMPILER = latexmk -f -jobname=main -pdfxe -xelatex="xelatex -8bit $(PERCENT)O $(PERCENT)S" -interaction=nonstopmode -file-line-error $(LATEXMK_OPTIONS) -output-directory=$(AUX_DIR)

DOCKER_RUN_COMMAND = docker run --name="$(DOCKER_CONTAINER_NAME)" --user $(shell id -u):$(shell id -g) --rm -it --mount src="$(current_dir)",target="$(current_dir)",type=bind -w "$(current_dir)" $(DOCKER_IMAGE_NAME) bash -c

# Auxiliary files management
AUX_DIR=./tmp
AUX_FILES_TO_DELETE=$(AUX_DIR) *.fls *latexmkrc*

# Shell
SHELL=/bin/bash

mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
current_dir := $(patsubst %/,%,$(dir $(mkfile_path)))

.PHONY: help setup-local build clean view watch docker_build_image ensure_in_repo ensure_docker_image_built $(ALLOWED_TYPES:%=%-zip) $(ALLOWED_TYPES) $(ALLOWED_DOCKER_CMD:%=d%)

# Main compilation target
build: $(TARGET).pdf

# Displays help options
help:
	@echo "Please run the make file with at least one of the following target:"
	@echo "-------------------------------"
	@echo " Working with Sharelatex (https://tex.zih.tu-dresden.de)"
	@echo "-------------------------------"
	@echo "  presentation-zip       : To create a presentation ZIP file to upload to Sharelatex."
	@echo "  poster-zip             : To create a poster ZIP file to upload to Sharelatex."
	@echo "  report-zip             : To create a poster ZIP file to upload to Sharelatex."
	@echo "-------------------------------"
	@echo " Working with local setup "
	@echo "-------------------------------"
	@echo "  setup-local            : To install required packages locally."
	@echo "  build                  : Compile and make pdf file."
	@echo "  watch                  : Continuous tracking and compiling."
	@echo "  install_latex_package  : Install latex packages in local tex-live installation"
	@echo "-------------------------------"
	@echo " Working with dockerized setup "
	@echo "-------------------------------"
	@echo "  docker_build_image     : Build docker image to be used for all the latex projects."
	@echo "  dbuild                 : Compile and make pdf file using docker container"
	@echo "  dwatch                 : Continuous tracking and compiling using docker container"
	@echo "-------------------------------"
	@echo " Copying template for working with local setup"
	@echo "-------------------------------"
	@echo "  presentation AT_LOCATION=/path/to/directory : To copy presentation template at AT_LOCATION."
	@echo "  poster AT_LOCATION=/path/to/directory       : To copy poster template at AT_LOCATION."
	@echo "  report AT_LOCATION=/path/to/directory       : To copy report template at AT_LOCATION."
	@echo "-------------------------------"
	@echo " Common commands               "
	@echo "-------------------------------"
	@echo "  view                   : To view compiled pdf using evince pdf reader"
	@echo "  clean                  : Remove auxiliary and compiled files like pdf."
	

# Compilation target
$(AUX_DIR)/$(TARGET).pdf: $(TARGET).tex
	@echo "Building latex project..."
	$(COMPILER) $<

# Setting up local packages
setup-local:
	@rm -rf /tmp/install-tl-*
	@sudo apt-get -y update && sudo apt-get install -y wget perl make inotify-tools libfontconfig1 fontconfig
	@wget -P /tmp/ $(TEXLIVE_INSTALL_MIRROR)/systems/texlive/tlnet/install-tl-unx.tar.gz
	@cd /tmp && tar -xvf install-tl-unx.tar.gz
	@cd /tmp/install-tl-20* && \
		perl install-tl --no-interaction --no-doc-install --no-src-install --scheme=basic --texdir="$(TEXLIVE_INSTALL_DIR)"
	@echo 'export PATH="$${PATH}:$(TEXLIVE_INSTALL_DIR)/bin/x86_64-linux"' > $(TEXLIVE_INSTALL_DIR)/.env
	@echo 'export MANPATH="$(TEXLIVE_INSTALL_DIR)/texmf-dist/doc/man"' >> $(TEXLIVE_INSTALL_DIR)/.env
	@echo 'export INFOPATH="$(TEXLIVE_INSTALL_DIR)/texmf-dist/doc/info"' >> $(TEXLIVE_INSTALL_DIR)/.env
	@grep -q "# Texlive installtion" "$${HOME}/.$(MYSHELL)rc" || echo  "# Texlive installtion" >> "$${HOME}/.$(MYSHELL)rc"
	@grep -q ". \"$(TEXLIVE_INSTALL_DIR)/.env\"" "$${HOME}/.$(MYSHELL)rc" || echo  ". \"$(TEXLIVE_INSTALL_DIR)/.env\"" >> "$${HOME}/.$(MYSHELL)rc"
	@PATH="$${PATH}:$(TEXLIVE_INSTALL_DIR)/bin/x86_64-linux" && \
		tlmgr update --self && \
		tlmgr install xetex latexmk tudscr newunicodechar csquotes isodate blindtext acronym setspace listings \
			bigfoot xstring enumitem esvect multirow footmisc biblatex substr biber zref microtype beamer extsizes \
			marginnote soulpos textpos wasysym adjustbox comment transparent pgfplots booktabs babel-german fontspec \
			datetime2 appendixnumberbeamer titlesec wrapfig printlen hyphenat collection-fontsrecommended caption \
			pdfcomment ragged2e fontawesome5 lstaddons&& \
		export OSFONTDIR=/usr/share/fonts && chmod -R o+w "$(TEXLIVE_INSTALL_DIR)/texmf-var" && fc-cache --really-force --verbose && \
		tlmgr update --self --all && \
		sudo apt-get autoremove && sudo apt-get autoclean && \
		rm -r /tmp/install-tl-* && \
		tlmgr --version
	@echo "Installation complete. Please reopen terminal for changes to apply."

$(TARGET).pdf: $(AUX_DIR)/$(TARGET).pdf
	cp "$<" $@

# Cleaning files
clean:
	rm -rf $(AUX_FILES_TO_DELETE) $(TARGET).pdf

# To view the compiled pdf
view: $(TARGET).pdf
	evince $<

# To continuously track the files and compile the pdf automatically
watch: build
	@echo "Watching files...";\
	while true; do \
		inotifywait -q -e modify -e move -e create -e delete -e attrib $(TRACK_FILES); \
		echo "Files changed, building...";\
		make clean; \
		make build LATEXMK_OPTIONS=$(LATEXMK_OPTIONS); \
		cp $(AUX_DIR)/$(TARGET).pdf .; \
	done

# Install latex packages on local machine
install_latex_package:
	@tlmgr update --self
	@while read i; do tlmgr install $$i; done < ./latex-packages-user.txt

# Build the docker image to be used for all the projects
docker_build_image: Dockerfile
	@echo "Building docker image: $(DOCKER_IMAGE_NAME)"
	@docker build -t $(DOCKER_IMAGE_NAME) --file $< .

ensure_docker_image_built:
	@if ! docker images "$(DOCKER_IMAGE_NAME)" --format="{{.Repository}},{{.CreatedAt}}" | grep -q "$(DOCKER_IMAGE_NAME)"; then \
		docker build -t "$(DOCKER_IMAGE_NAME)" --file Dockerfile .; \
	fi

# Common target for dwatch (build continuously) and dbuild (build once)
$(ALLOWED_DOCKER_CMD:%=d%): d%: ensure_docker_image_built
	$(DOCKER_RUN_COMMAND) "make $* LATEXMK_OPTIONS=$(LATEXMK_OPTIONS)"

ensure_in_repo:
	@if ! test -e Dockerfile; then \
		echo "Cannot create a new poster/presentation/report from existing one (Run this command from the original bundle)"; \
		exit 1; \
	fi

ensure_target_location_set_%:
	@if test -z "$(AT_LOCATION)"; then \
		echo "Did you miss parameter AT_LOCATION, as in 'make $(patsubst ensure_target_location_set_%,%,$@) AT_LOCATION=/tmp/my-$(patsubst ensure_target_location_set_%,%,$@)'?"; \
		exit 1; \
	fi

$(ALLOWED_TYPES:%=%-zip): %-zip:
	@echo $*
	@zip -rq $* $*
	@echo "Open $(SHARELATEX_URL) in your browser, select New Project -> Upload Project and upload $*.zip"

$(ALLOWED_TYPES): %: ensure_in_repo ensure_target_location_set_% ensure_docker_image_built
	@mkdir -p "$(AT_LOCATION)"
	@cp -r $*/* "$(AT_LOCATION)"
	@cp Makefile "$(AT_LOCATION)"
