%$Id: a0poster.cls,v 1.5 1999/04/05 14:49:56 mjf Exp $
%% 
%% This is file `a0poster.cls'
%% 
%% Copyright (C) 07.05.97 Gerlinde Kettl and Matthias Weiser
%%
%% Problems, bugs and comments to 
%% gerlinde.kettl@physik.uni-regensburg.de
%% 
%% changed textwidths and margins to cope with printable area and
%% frame.  Make sure that the offsets are set to -1in!
%% Also changed scaling for a0->a4, since this was wrong.

%% This version changed by Hugh Pumphrey on 4.5.1999. Every instance
%% of the string ``draft'' replaced by the string ``preview''. This gives 
%% an a4 size preview but includes the postscript figures

% \ProvidesClass{a0poster}[1997/05/07 v1.21b a0poster class (GK, MW)]
%===============================================================================
% Modified by: Apurv Deepak Kulkarni (27 September 2022)
%===============================================================================
%TODO: Check margins with original MS ppt template
%TODO: Make a different way to mention authors in footer (which will not have all the authors mentioned under the title)
%TODO: Check landscape mode
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{scadsai-poster-template}[2024/05/27 Custom Class File]

\RequirePackage{kvoptions}
\SetupKeyvalOptions{family=scadsai,prefix=scadsai@}

\newif\ifportrait
\newif\ifanullb
\newif\ifanull    % a0 paper size
\newif\ifaeins
\newif\ifazwei
\newif\ifadrei
\newif\ifpreview

\newcount\xkoord
\newcount\ykoord
\newcount\xscale
\newcount\yscale

\newif\ifenglish
\newif\ifgerman

\DeclareOption{a0b}{
    \anullbtrue
    \xkoord=2592 % big points (1 bp=1/72 inch)
    \ykoord=3666 % big points (1 bp=1/72 inch)
    \xscale=23
    \yscale=23
}
\DeclareOption{a0}{
    \anulltrue\anullbfalse
    \xkoord=2380 % big points (1 bp=1/72 inch)
    \ykoord=3368 % big points (1 bp=1/72 inch)
    \xscale=25
    \yscale=25
}
\DeclareOption{a1}{
    \aeinstrue\anullbfalse
    \xkoord=1684 % big points (1 bp=1/72 inch)
    \ykoord=2380 % big points (1 bp=1/72 inch)
    \xscale=3
    \yscale=3
}
\DeclareOption{a2}{
    \azweitrue\anullbfalse
    \xkoord=1190 % big points (1 bp=1/72 inch)
    \ykoord=1684 % big points (1 bp=1/72 inch)
    \xscale=4
    \yscale=4
}
\DeclareOption{a3}{
    \adreitrue\anullbfalse
    \xkoord=842  % big points (1 bp=1/72 inch)
    \ykoord=1190 % big points (1 bp=1/72 inch)
    \xscale=6
    \yscale=6
}
\DeclareOption{landscape}{\portraitfalse}
\DeclareOption{portrait}{\portraittrue}
\DeclareOption{preview}{\previewtrue}
\DeclareOption{final}{\previewfalse}
\DeclareOption{english}{
    \englishtrue
    \germanfalse
}
\DeclareOption{german}{
    \germantrue
    \englishfalse
}
\newif\if@useSectionNum
\DeclareOption{useSectionNumber}{
    \@useSectionNumtrue
}
\newif\if@useSectionTitleCenter
\DeclareOption{useSectionTitleCenter}{
    \@useSectionTitleCentertrue
}
\newif\ifcomputetimeused
\computetimeusedtrue
\DeclareOption{nocomputetimeused}{
    \computetimeusedfalse
}

\ProcessOptions
\LoadClass{article}

%
%===============================================================================
% Package setup (except language and font)
%===============================================================================
\usepackage[svgnames]{xcolor}
\usepackage{textpos}
\usepackage{fancyhdr}
\usepackage{tabularx}
\usepackage{pgffor,etoolbox}
\usepackage{multirow}
\usepackage{expl3}
\usepackage{xparse}
\usepackage[explicit]{titlesec}
\RequirePackage{tikz}
\usetikzlibrary{fadings,positioning,calc}
\usepackage{amsmath}
\usepackage{xstring}
\usepackage{isodate}

\renewcommand{\labelitemi}{$\vcenter{\hbox{\textcolor{scadsaiblue}{\small$\blacksquare$}}}$}
\renewcommand{\labelitemii}{$\vcenter{\hbox{\textcolor{scadsaiblue}{\footnotesize$\blacksquare$}}}$}
\renewcommand{\labelitemiii}{$\vcenter{\hbox{\textcolor{scadsaiblue}{\tiny$\blacksquare$}}}$}

%===============================================================================
% Language and font setup
%===============================================================================
\ifenglish
	\RequirePackage[main=english,ngerman]{babel}
\else
	\RequirePackage[english,main=ngerman]{babel}
\fi
%
%%% Language vocabulary
\newcommand*{\englishvocabulary}{%
	\newcommand*{\scadsai@vocslide}{Slide}
	\newcommand*{\scadsai@vocof}{of}
	\newcommand*{\scadsai@vocpart}{Part}
	\newcommand*{\scadsai@vocagenda}{Agenda}
	\newcommand*{\scadsai@vocauthors}{Authors}
	\newcommand*{\scadsai@voccontact}{Contact}
	\newcommand*{\scadsai@vocpartners}{Partners}
	\newcommand*{\scadsai@vocsponsoredby}{Sponsored by}
	\newcommand*{\scadsai@vocacknowledgement}{The authors gratefully acknowledge the GWK support for funding this project by providing computing time through the Center for Information Services and HPC (ZIH) at TU Dresden on HRSK-II.}
}
\newcommand*{\germanvocabulary}{%
	\newcommand*{\scadsai@vocslide}{Folie}
	\newcommand*{\scadsai@vocof}{von}
	\newcommand*{\scadsai@vocpart}{Teil}
	\newcommand*{\scadsai@vocagenda}{Gliederung}
	\newcommand*{\scadsai@vocauthors}{Autoren}
	\newcommand*{\scadsai@voccontact}{Kontakt}
	\newcommand*{\scadsai@vocpartners}{Partners}
	\newcommand*{\scadsai@vocsponsoredby}{Gefördert von}
	\newcommand*{\scadsai@vocacknowledgement}{Die Autoren bedanken sich bei der GWK für die Förderung dieses Projekts durch die Bereitstellung von Rechenzeit durch das Zentrum für Informationsdienste und HPC (ZIH) der TU Dresden auf HRSK-II.}
}
%
% \RequirePackage{lmodern}
% \usefonttheme{professionalfonts}
% % Helvet package as an alternative to Barlow font for headlines, title pages and footers
% \usepackage{helvet}
% % Open sans for the content slides
% \RequirePackage[defaultsans,scale=.95]{opensans} %scales the letter spacing, default refers to the printing style for numbers
% \renewcommand{\sfdefault}{cmss}
% \renewcommand*\familydefault{\sfdefault} %family open sans
% \RequirePackage[T1]{fontenc}
%
\usepackage{fontspec}
\defaultfontfeatures{Ligatures=TeX}
\defaultfontfeatures[barlow]{
	Path=./fonts/,
    Extension = .ttf , 
    UprightFont = Barlow-Regular,
    BoldFont = Barlow-Bold,
    ItalicFont = Barlow-Italic,
    BoldItalicFont = Barlow-BoldItalic,
    FontFace = {sb}{n}{Barlow-SemiBold},
    FontFace = {md}{n}{Barlow-Medium},
}
\defaultfontfeatures[opensans]{
	Path=./fonts/,
    Extension = .ttf, 
    UprightFont = OpenSans-Regular,
    BoldFont = OpenSans-Bold,
    ItalicFont = OpenSans-Italic,
    BoldItalicFont = OpenSans-BoldItalic,
    FontFace = {sb}{n}{OpenSans-Semibold},
    FontFace = {lt}{n}{OpenSans-Light},
		FontFace = {lt}{it}{OpenSans-LightItalic},
    FontFace = {r}{n}{OpenSans-Regular},
}
%
% \fontspec{opensans}
% \setmainfont{EB Garamond}
\newfontfamily\opensansfont{opensans}
\newfontfamily\barlowfont{barlow}
\setmainfont{opensans}
%
% Setting some font types 
\DeclareRobustCommand\ebseries{\fontseries{eb}\selectfont} % extra bold
\DeclareRobustCommand\sbseries{\fontseries{sb}\selectfont} % semi bold
\DeclareRobustCommand\ltseries{\fontseries{lt}\selectfont}  % light
\DeclareRobustCommand\clseries{\fontseries{cl}\selectfont} 
\DeclareRobustCommand\mseries{\fontseries{md}\selectfont} 
\DeclareRobustCommand\rseries{\fontseries{r}\selectfont} 
% Aliasing commands to regular text commands
\DeclareTextFontCommand{\texteb}{\ebseries} % extra bold
\DeclareTextFontCommand{\textsb}{\sbseries} % semi bold
\DeclareTextFontCommand{\textlt}{\ltseries} % light
\DeclareTextFontCommand{\textcl}{\clseries} % condensed light text. Not working ??
\DeclareTextFontCommand{\text}{\rseries} % condensed light text. Not working ??
%
% Redefining font size definition (note: only for A0 size)
% TODO: Figure out how to change these font sizes as per paper size
% 2nd value is based on original proportion from following link
% https://tex.stackexchange.com/questions/322256/what-is-the-second-value-in-fontsize-for
\renewcommand{\tiny}{\fontsize{12}{14}\selectfont}
\renewcommand{\scriptsize}{\fontsize{14.4}{18}\selectfont}   
\renewcommand{\footnotesize}{\fontsize{17.28}{22}\selectfont}
\renewcommand{\small}{\fontsize{20.74}{25}\selectfont}
% \renewcommand{\normalsize}{\fontsize{24.88}{30}\selectfont}
\renewcommand{\normalsize}{\fontsize{28}{33.6}\selectfont}
\renewcommand{\large}{\fontsize{29.86}{37}\selectfont}
\renewcommand{\Large}{\fontsize{35.83}{45}\selectfont}
% \renewcommand{\LARGE}{\fontsize{43}{54}\selectfont}
\renewcommand{\LARGE}{\fontsize{44}{56.9}\selectfont}
% \renewcommand{\huge}{\fontsize{51.6}{64}\selectfont}
\renewcommand{\huge}{\fontsize{54}{67.5}\selectfont}
\renewcommand{\Huge}{\fontsize{61.92}{77}\selectfont}
\newcommand{\veryHuge}{\fontsize{74.3}{93}\selectfont}
\newcommand{\VeryHuge}{\fontsize{89.16}{112}\selectfont}
\newcommand{\VERYHuge}{\fontsize{107}{134}\selectfont}
%
% Poster font setup
\newcommand\setthemainfont{\opensansfont\rseries\normalsize}
\newcommand{\setmaintitlefont}{\barlowfont\sbseries\VeryHuge}
\newcommand{\setmaintitlesubtitlefont}{\barlowfont\sbseries\huge}
\newcommand\setmaintitleauthorfont{\opensansfont\ltseries\normalsize}
\newcommand\setmaintitleaffiliationfont{\opensansfont\ltseries\small}
\newcommand\setsectiontitlefont{\barlowfont\mseries\LARGE}
\newcommand\setsubsectiontitlefont{\barlowfont\mseries\Large}
\newcommand\setfooterauthorfont{\opensansfont\rseries\normalsize}
\newcommand\setfooterauthoremailfont{\setfooterauthorfont\normalsize}
\newcommand\setfooterunivfont{\opensansfont\fontsize{32}{32}\selectfont}
\newcommand\setfootertitlefont{\barlowfont\sbseries\normalsize\MakeUppercase}
%
%
%===============================================================================
% Defining Paper sizes
%===============================================================================
\ifanullb
	\setlength{\paperwidth}{129.3cm} %% 36 * sqrt(2) in
	\setlength{\paperheight}{91.4cm} %% 36 in
	\setlength{\textwidth}{119.3cm} %% paperwidth - (5cm + 5cm)
	\setlength{\textheight}{81.4cm} %% paperheight - (5cm + 5cm)
\else\ifanull
	\setlength{\paperwidth}{118.82cm}
	\setlength{\paperheight}{83.96cm}
	\setlength{\textwidth}{108.82cm} %% paperwidth - (5cm + 5cm)
	\setlength{\textheight}{73.96cm} %% paperheight - (5cm + 5cm)
\else\ifaeins
	\setlength{\paperwidth}{83.96cm}
	\setlength{\paperheight}{59.4cm}
	\setlength{\textwidth}{79.96cm}
	\setlength{\textheight}{55.4cm}
\else\ifazwei
	\setlength{\paperwidth}{59.4cm}
	\setlength{\paperheight}{41.98cm}
	\setlength{\textwidth}{55.4cm}
	\setlength{\textheight}{37.98cm}
\else\ifadrei
	\setlength{\paperwidth}{41.98cm}
	\setlength{\paperheight}{29.7cm}
	\setlength{\textwidth}{37.98cm}
	\setlength{\textheight}{25.7cm}
\else\ifavier
	\setlength{\paperwidth}{29.7cm}
	\setlength{\paperheight}{21.0cm}
\else
	\relax
\fi
\fi
\fi
\fi
\fi
%
% Defining skips
\setlength\smallskipamount{6pt plus 2pt minus 2pt}
\setlength\medskipamount{12pt plus 4pt minus 4pt}
\setlength\bigskipamount{24pt plus 8pt minus 8pt}
%
\setlength\abovecaptionskip{25pt}
\setlength\belowcaptionskip{0pt}
\setlength\abovedisplayskip{25pt plus 6pt minus 15 pt}
\setlength\abovedisplayshortskip{0pt plus 6pt}
\setlength\belowdisplayshortskip{13pt plus 7pt minus 6pt}
\setlength\belowdisplayskip\abovedisplayskip
%
%
%===============================================================================
% Margin setup
%===============================================================================
\ifanull
	% \setlength{\hoffset}{-0.54cm} %1inch-x= 2.54cm-x = required margin->2cm => x = -0.54cm margin
	% \setlength{\voffset}{-1in}
	\newlength{\myleftmargin}\setlength{\myleftmargin}{5cm}
	\setlength{\oddsidemargin}{\dimexpr\myleftmargin-1in\relax} %% %1inch+x=2.54cm+x = required margin->2cm => x=-0.54cm margin
	%% (landscape) + 2 cm from border
	\newlength{\mytopmarginInit}\setlength{\mytopmarginInit}{11cm}
	\newlength{\mytopmargin}\setlength{\topmargin}{\dimexpr\mytopmarginInit-1in\relax} %% 3 cm for unprintable at top
	%% (landscape) + 2 cm from border
	
	\setlength{\headheight}{0cm}
	\setlength{\headsep}{0cm}
	
	\setlength{\marginparsep}{0cm}
	\setlength{\marginparwidth}{0cm}
	
	\newlength{\footerheight}
	\ifportrait
		\setlength{\footerheight}{210mm}
	\else
		\setlength{\footerheight}{20cm}
	\fi
	\setlength{\footskip}{11mm}
\else\ifavier
	% \setlength{\hoffset}{-0.54cm} %1inch-x= 2.54cm-x = required margin->2cm => x = -0.54cm margin
	% \setlength{\voffset}{-1in}
	\newlength{\myleftmargin}\setlength{\myleftmargin}{0.5cm}
	\newlength{\mytopmargin}\setlength{\mytopmargin}{7cm}
	\setlength{\oddsidemargin}{\dimexpr\myleftmargin-1in\relax} %% %1inch+x=2.54cm+x = required margin->2cm => x=-0.54cm margin
	%% (landscape) + 2 cm from border
	\setlength{\topmargin}{\dimexpr\mytopmargin-1in\relax} %% 3 cm for unprintable at top
	%% (landscape) + 2 cm from border
	
	% \setlength{\headheight}{3.5cm}
	\setlength{\headheight}{0.15\paperheight}
	% \addtolength{\topmargin}{-18.90369pt}
	\setlength{\headsep}{0.001\paperheight}
	
	\setlength{\marginparsep}{0cm}
	\setlength{\marginparwidth}{0cm}
	
	\newlength{\footerheight}
	\ifportrait
	\setlength{\footerheight}{7cm}
	\else
	\setlength{\footerheight}{5cm}
	\fi
	\setlength{\footskip}{5pt}
	%\setlength{\footskip}{0cm}
\else\relax\fi
%
\ifportrait
\newdimen\tausch
\setlength{\tausch}{\paperwidth}
\setlength{\paperwidth}{\paperheight}
\setlength{\paperheight}{\tausch}
\setlength{\tausch}{\textwidth}
\setlength{\textwidth}{\textheight}
\setlength{\textheight}{\tausch}
\else\relax
\fi
%
\setlength{\textheight}{\dimexpr\paperheight-\topmargin-\headsep-\headheight-\footerheight-\footskip\relax}
%\setlength{\textheight}{\dimexpr\paperheight-\topmargin-\headsep-\headheight-\footerheight\relax}
\setlength{\textwidth}{\dimexpr\paperwidth-2\myleftmargin\relax}
%
%
\newcommand{\spacechar}{\texttt{\char32}}
%
%
%===============================================================================
% Theme 1
%===============================================================================
%scads.ai colors
\definecolor{scadsaiblack}{HTML}{1a1a1a}
\definecolor{scadsaitorquise}{HTML}{009299}
\definecolor{scadsaiblue}{HTML}{0074AC}
\definecolor{scadsaidarkblue}{HTML}{004B6F}
\definecolor{scadsaidarkgrey}{HTML}{7F7F7F}
\definecolor{scadsaigreen}{HTML}{71BD56}
\definecolor{scadsaigrey}{HTML}{7A7B7A}
\definecolor{scadsailightgrey}{HTML}{DADADA}
\definecolor{scadsaimiddleblue}{HTML}{005277}
\definecolor{scadsaiwhite}{HTML}{ffffff}
\colorlet{tudblue}{scadsaidarkblue}%
% basic colors
\definecolor{white}{gray}{1.00}
\definecolor{grey}{gray}{0.65}
\definecolor{black}{gray}{0.00}
\definecolor{hks41}{cmyk/RGB}{1,0.7,0.1,0.5/0,48,94}%TU blue primary cd color
\definecolor{hks92}{cmyk/RGB}{0.1,0,0.05,0.65/114,120,121}%TU grey secondary cd color - grey
\definecolor{hks44}{cmyk/RGB}{1,0.5,0,0/0,106,179}%highlight color 1st category cd   - brightblue
\definecolor{cyan}{cmyk/RGB}{1,0,0,0/0,158,224}%highlight color 1st category cd      - cyan
\definecolor{hks65}{cmyk/RGB}{0.65,0,1,0/106,176,35}%highlight color 2nd category cd - bright green
\definecolor{hks57}{cmyk/RGB}{1,0,0.9,0.2/0,125,64}%highlight color 2nd category cd - dark green
\definecolor{hks33}{cmyk/RGB}{0.5,1,0,0/147,16,126}%highlight color 2nd category cd - purple
\definecolor{hks36}{cmyk/RGB}{0.8,0.9,0,0/84,55,138}%highlight color 2nd category cd - violet
\definecolor{hks07}{cmyk/RGB}{0,0.6,1,0/238,127,0}%highlight color 2nd category cd - orange

\newlength{\gradientrulethickness}
\newlength{\gradientrulelength}
\newlength{\cxpos}
\newcommand{\gradient}[3]{\noindent% 1-color,2-thickness,3-length,4-center xpos
	\setlength{\gradientrulethickness}{#2}
	\setlength{\gradientrulelength}{#3}
	%	\setlength{\cxpos}{#4}
	%	\hspace*{\dimexpr\cxpos-0.5\gradientrulelength\relax} %
	%	\hspace*{\cxpos} %
	\begin{tikzpicture}[baseline]
	%	\fill[#1,path fading=west] ([yshift=0,xshift=\cxpos]0,0) rectangle (-0.5\gradientrulelength,\gradientrulethickness);
	\fill[#1,path fading=west] (0,0) rectangle (-0.5\gradientrulelength,\gradientrulethickness);
	%	\fill[#1,path fading=west] (-0.5\gradientrulelength,0) rectangle (0,\gradientrulethickness);
	\fill[#1,path fading=east] (0,0) rectangle (0.5\gradientrulelength,\gradientrulethickness);
	\end{tikzpicture}%
}
%

\usepackage[colorlinks=true,urlcolor=scadsaiblue,implicit=false]{hyperref}

% \newcommand\title[1]{\renewcommand\@title{#1}}
% \def\@title{}
\newcommand\subtitle[1]{\renewcommand\@subtitle{#1}}
\def\@subtitle{}
\newcommand\affiliation[1]{\renewcommand\@affiliation{#1}}
\def\@affiliation{}
%
% Author under the title
\newcommand\myauthor[1]{\renewcommand\@myauthor{#1}}
\def\@myauthor{}
%
% Research area
\newcommand\myresearcharea[1]{\renewcommand\@myresearcharea{#1}}
\def\@myresearcharea{disable}
%
% Author details for the footer
\newcommand\myfooterauthor[1]{\renewcommand\@myfooterauthor{#1}}
\def\@myfooterauthor{}
\newcommand\myfooteremail[1]{\renewcommand\@myfooteremail{#1}}
\def\@myfooteremail{}
\newcommand\myfooteruni[1]{\renewcommand\@myfooteruni{#1}}
\def\@myfooteruni{}
%
\newcommand\adTitle[1]{\renewcommand\@adTitle{#1}}
\def\@adTitle{}
% \newcommand\adSubTitle[1]{\renewcommand\@adSubTitle{#1}}
% \def\@adSubTitle{}
%
\def\tand{&} % '&' for the footer table
%
\newcommand\mytablecontentsAuthorName{}
\newcommand\mytablecontentsAuthorEmail{}
\newcommand\mytablecontentsAuthorUni{}
%
\newcommand\authornum{}
\newcommand{\myauthornum}[1]{\renewcommand*{\authornum}{#1}}
%
% Need 'expl3' and 'xparse' packages
\ExplSyntaxOn
\cs_generate_variant:Nn \clist_count:n { V }
\cs_new_eq:NN \countItems \clist_count:V
\ExplSyntaxOff
%
\newlength{\rulewidth}
\setlength{\rulewidth}{1pt}
%
%
%===============================================================================
% Background design
%===============================================================================
\newcommand{\setScadsaiPosterThemeOne}{
	%
	%	\begin{tikzpicture}[remember picture, overlay]
	%%	\node [fill, rectangle, left color=scadsaidarkblue, right color=white, anchor=north west, minimum width=0.07\paperwidth, minimum height=\paperheight, yshift=0cm, xshift=0cm] (box) at (current page.north west){};
	%%	\node [fill, rectangle, right color=scadsaidarkblue, left color=white, anchor=north east, minimum width=0.07\paperwidth, minimum height=\paperheight, yshift=0cm, xshift=-\hoffset] (box) at (current page.north east){};
	%%	\node [fill, rectangle,	bottom color=scadsaidarkblue, middle color=scadsaimiddleblue, top color=scadsaiblue, anchor=north west, minimum width=\paperwidth, minimum height=\paperheight, yshift=10cm] (box) at (current page.north west){};
	%	\end{tikzpicture}
	%
	% ScadsAI Logo
	\tikz[overlay,remember picture]{
		\node[opacity=1, anchor=north west, xshift=\myleftmargin, yshift=-35mm] at (current page.north west) {
			\includegraphics[width=124mm, trim=0pt 0pt 0pt 0pt, clip]{templatepictures/logos/scads-logo.png}
		};
	}
	
	% Top right strings
	\tikz[overlay,remember picture]{
		\node[opacity=1, anchor=north east, xshift=10pt, yshift=10pt] at (current page.north east) {
			\includegraphics[width=518mm, trim=3cm 0pt 24.912mm 153mm, clip, angle=0]{templatepictures/design/strings-orig.jpeg}
		};
		
	}
	% bottom waves
	\tikz[overlay,remember picture]{
		\node[opacity=1, anchor=south, xshift=0pt, yshift=-15pt] at (current page.south) {
			\ifportrait
			\includegraphics[width=\paperwidth, trim=0pt 0pt 0pt 0pt, clip, angle=0]{templatepictures/design/bottom_waves.png}
			\else
			\includegraphics[width=\paperwidth, height=0.22\footerheight, trim=0pt 0pt 0pt 0pt, clip, angle=0]{templatepictures/design/bottom_waves.png}
			\fi
		};
	}
}
%
\newcommand{\makebackground}{\setScadsaiPosterThemeOne}
%
% Set Rresearch area image
\newcommand{\getResearchAreaImage}{%
	\IfStrEq{\@myresearcharea}{disable}{}{%
		\begin{tikzpicture}%
			\node {%
				\IfStrEq{\@myresearcharea}{all}{\includegraphics[width=\textwidth]{templatepictures/research-area/png_structure-all.png}}{}%
				\IfStrEq{\@myresearcharea}{ab}{\includegraphics[width=\textwidth]{templatepictures/research-area/png_structure-ab.png}}{}%
				\IfStrEq{\@myresearcharea}{am}{\includegraphics[width=\textwidth]{templatepictures/research-area/png_structure-am.png}}{}%
				\IfStrEq{\@myresearcharea}{bd}{\includegraphics[width=\textwidth]{templatepictures/research-area/png_structure-bd.png}}{}%
				\IfStrEq{\@myresearcharea}{arc}{\includegraphics[width=\textwidth]{templatepictures/research-area/png_structure-archt.png}}{}%
				\IfStrEq{\@myresearcharea}{res}{\includegraphics[width=\textwidth]{templatepictures/research-area/png_structure-resp.png}}{}%
			};%
		\end{tikzpicture}%
	}%
}
%
%
%===============================================================================
% Title, Subtitle, Author
%===============================================================================
\newlength{\titleWidth}
\newlength{\titleResearchAreaSpacingWidth}
\newlength{\researchAreaImagewidth}
%
\setlength{\titleWidth}{0.70\textwidth}
\setlength{\titleResearchAreaSpacingWidth}{3cm}
\setlength{\researchAreaImagewidth}{\dimexpr\textwidth-\titleWidth-\titleResearchAreaSpacingWidth\relax}
\newlength{\titleVertSpacing}\setlength{\titleVertSpacing}{1cm}
%
\renewcommand{\maketitle}{%
	\IfStrEq{\@myresearcharea}{disable}{
		\setlength{\titleWidth}{\textwidth}
		\setlength{\titleResearchAreaSpacingWidth}{0cm}
		\setlength{\researchAreaImagewidth}{0cm}
		\begin{minipage}[t]{\titleWidth}\vspace*{0mm}%
	}{
		\begin{minipage}[t]{\titleWidth}\vspace*{0mm}%
	}
		% Title
		{\color{tudblue}\setmaintitlefont\selectfont\@title\par\vspace{\titleVertSpacing}}
		%
		% Subtitle
		\ifx\@subtitle\empty\else{\color{scadsaigreen}\huge\opensansfont\ltseries\selectfont\strut\@subtitle\strut\par\vspace{\titleVertSpacing}}\fi
		%
		% Author(s)
		{\noindent\ignorespaces\setmaintitleauthorfont%
			\foreach \i [count=\x] in \@myauthor%
			{\i%
			\ifnum\x=\countItems{\@myauthor}%
			\else%
				,\spacechar%
			\fi}%
		}\par%
		{\noindent\ignorespaces\setmaintitleaffiliationfont\@affiliation}%
	\end{minipage}%
	\hspace*{\titleResearchAreaSpacingWidth}%
	%
	% Research Area
	\begin{minipage}[t]{\researchAreaImagewidth}\vspace*{0mm}%
		\getResearchAreaImage%
	\end{minipage}%
	%
	\vspace*{3cm}%
	%
}
%
%
%===============================================================================
% Footer
%===============================================================================
\newlength{\footertablefirstcolwidth}
\newlength{\footerlogotablewidth}
\newlength{\footerlogotablewidtheach}
\newlength{\footerlogowidth}
\newlength{\footerlogoheight}
\newlength{\footerauthorwidthtotal}
\newlength{\footerauthorwidtheach}
\newlength{\footersponsorwidth}
\newlength{\tmplogospace}
%
%
\setlength{\footertablefirstcolwidth}{65.509mm}
\setlength{\footersponsorwidth}{0.52\textwidth}
\setlength{\footerauthorwidthtotal}{\dimexpr\textwidth-\footertablefirstcolwidth-\footersponsorwidth\relax}
%
\setlength{\footerlogotablewidth}{\dimexpr\textwidth-\footertablefirstcolwidth\relax}
\ifportrait
%\setlength{\footerlogowidth}{0.05\footerlogotablewidth}
\setlength{\footerlogoheight}{20mm}
\setlength{\footerlogotablewidtheach}{110mm}
\else
\setlength{\footerlogowidth}{0.07\footerlogotablewidth}
\fi
%	
% Footer portrait
\newcommand{\footercontactheader}{{\color{tudblue}\setfootertitlefont\scadsai@voccontact}}
\newcommand{\footercontactdetails}{
\ifnum\countItems{\@myfooterauthor}=1%
	\setlength{\footerauthorwidtheach}{\linewidth}%
\else%
	\ifnum\countItems{\@myfooterauthor}=2%
		\setlength{\footerauthorwidtheach}{0.5\linewidth} % max 2 (1/2=0.5) authors can fit in portrait mode
	\else%
		\setlength{\footerauthorwidtheach}{0.33333333333333333\linewidth} % max 3 (1/3=0.333) authors can fit in portrait mode
	\fi%
\fi%
	\begin{tabular}[t]{*{\countItems{\@myfooterauthor}}{>{\ignorespaces\noindent\raggedright\arraybackslash}p{\footerauthorwidtheach}}}% arbitrary number of columns below 3
		\mytablecontentsAuthorUni\\[5pt]
		\noindent\mytablecontentsAuthorName\\[5pt]
		\mytablecontentsAuthorEmail\\
	\end{tabular}%
}
\newcommand{\sponsor}{%
	\begin{tikzpicture}[baseline=(a.base), anchor=west]
	\node[align=right, text width=20ex] (a) at (0,0) {\color{tudblue}\setfootertitlefont\scadsai@vocsponsoredby};
	\node[right=2.5pt of a.north east, anchor=north west] (b) {\includegraphics[height=2\footerlogoheight]{templatepictures/logos/logo-bmbf-neu.png}};
	\node[right=2.5pt of b.north east, anchor=north west] (c) {\includegraphics[height=2\footerlogoheight]{templatepictures/logos/logo-sachsen.png}};
	\node[right=2.5pt of c.north east, anchor=north west] (d) {\includegraphics[height=1.9\footerlogoheight]{templatepictures/logos/Text_Bund_Land.png}};
	\node[right=0pt of b.south east, anchor=south east] (d) {\opensansfont\small FKZ:ScaDS.AI};
	\end{tikzpicture}%
}
\newcommand{\footerpartners}{
	\begin{tabularx}{\footerlogotablewidth}{
			>{\centering\arraybackslash}m{\footerlogotablewidtheach}
			>{\centering\arraybackslash}m{\footerlogotablewidtheach}
			>{\centering\arraybackslash}m{1.0\footerlogotablewidtheach}
			>{\centering\arraybackslash}m{1.0\footerlogotablewidtheach}
			>{\centering\arraybackslash}m{1.25\footerlogotablewidtheach}
			>{\centering\arraybackslash}m{0.75\footerlogotablewidtheach}
		}%
		% 1st row of logos
		\includegraphics[height=\footerlogoheight]{templatepictures/logos/logo-tud.png}&%
		\includegraphics[height=1.4\footerlogoheight]{templatepictures/logos/logo-ul.png} &%
		\includegraphics[height=1.2\footerlogoheight]{templatepictures/logos/logo-mpi-cbg.jpg} &%
		\includegraphics[height=1.5\footerlogoheight, trim=10pt 10pt 10pt 10pt, clip]{templatepictures/logos/logo-ioer.png} &%
		\includegraphics[height=\footerlogoheight]{templatepictures/logos/logo-ufz.png} &%
		\includegraphics[height=1.2\footerlogoheight]{templatepictures/logos/logo-hzdr.png}\\%
		% 2nd row of logos
		\includegraphics[height=1.2\footerlogoheight]{templatepictures/logos/logo-infai-compact.pdf}&%
		\includegraphics[height=0.8\footerlogoheight]{templatepictures/logos/logo-mpi-kn.jpeg}&%
		\includegraphics[height=\footerlogoheight, trim=0pt 0pt 300pt 30pt, clip]{templatepictures/logos/logo-fr-izi.jpg}&%
		\includegraphics[height=\footerlogoheight, trim=0pt 40pt 0pt 40pt, clip]{templatepictures/logos/logo-fr-iais.jpg}&%
		\includegraphics[height=1.4\footerlogoheight]{templatepictures/logos/logo-dlr-new.pdf}&%
		\includegraphics[height=1.3\footerlogoheight, trim=20pt 10pt 10pt 0pt, clip]{templatepictures/logos/logo-mpi-mn.png}\\%
	\end{tabularx}
}
\newcommand{\qrcode}{\includegraphics[width=\footertablefirstcolwidth]{templatepictures/logos/qr-code.png}}
%
%\newcommand\topalign[1]{%
%	\setbox0\hbox{#1}%
%	\raisebox{\dimexpr-\ht0+\dp0\relax}{\usebox0}
%}
%
\newcommand{\makefooter}{
	%
	% Green rule
	{\color{scadsaigreen}\rule{\textwidth}{\rulewidth}}\\ %\par
	\vspace*{\footskip}
	%
	%	\vspace*{-5pt}
	%
	\begin{tabularx}{\textwidth}%
		{%
			>{\ignorespaces\noindent}p{\footertablefirstcolwidth}%
			>{\raggedright\arraybackslash}p{\footerauthorwidthtotal}%
			>{\raggedright\arraybackslash}p{\footersponsorwidth}%
		}
		%
		% 1st row of the footer
		%
		\footercontactheader & \footercontactdetails & \sponsor\\[4.6cm]%
		%
		% 2nd row of the footer
		%
		% QR code, Logos tableline
		%		\qrcode & \multicolumn{2}{>{\raggedright\ignorespaces\noindent}X}{\raisebox{10pt}{\footerpartners}}\\%
		\qrcode & \multicolumn{2}{c}{\raisebox{25mm}{\footerpartners}}\\%
	\end{tabularx}
	%
	\centering\vspace*{0pt}\opensansfont\footnotesize\scadsai@vocacknowledgement
}

\pagestyle{fancy}
\fancyhf{}%
% Header
\fancyhead[L]{}%
\renewcommand{\headrulewidth}{0cm}
%Footer
\fancyfoot[L]{\makefooter}%
\renewcommand{\footrulewidth}{0cm}
%
%===============================================================================
% Section definition: Requrires [explicit]{titlesec}
%===============================================================================
% For general section
\titleformat%
{\section}%command
[block]%shape
{\setsectiontitlefont}%format
{}%label
{0pt}%separator
{% before code
	{%
		\color{tudblue}%
		\if@useSectionTitleCenter%
			\hfill\if@useSectionNum\thesection~~\fi\centering{#1}\hfill%
		\else%
			\if@useSectionNum\thesection~~\fi{#1}%
		\fi%
	}
}
[%after code
    \vspace*{.3cm}{\color{scadsaigreen}\hrule}\vspace*{.4cm}%
]
%
% For having numberless reference section title
\titleformat{name=\section,numberless}[block]{\setsectiontitlefont}{}{0pt}{
	{\color{tudblue}\if@useSectionTitleCenter\hfill\centering{#1}\hfill\else{#1}\fi}
}[\vspace*{.3cm}{\color{scadsaigreen}\hrule}\vspace*{.4cm}]
%
% For general subsection
\titleformat%
{\subsection}%command
[block]%shape
{\setsubsectiontitlefont}%format
{}%label
{0pt}%separator
{% before code
	{%
		\color{tudblue}%
		\if@useSectionTitleCenter%
			\hfill\if@useSectionNum\thesubsection~~\fi\centering{#1}\hfill%
		\else%
			\if@useSectionNum\thesubsection~~\fi{#1}%
		\fi%
	}
}
[%after code
    \vspace*{.4cm}%
]
%
% For having numberless reference subsection title
\titleformat{name=\subsection,numberless}[block]{\setsubsectiontitlefont}{}{0pt}{
	{\color{tudblue}\if@useSectionTitleCenter\hfill\centering{#1}\hfill\else{#1}\fi}
}[\vspace*{.4cm}]
%
%
%
\AtBeginDocument{
	% Removes the ordinal numbering of the day
	\cleanlookdateon
    \setthemainfont
    % Setting up language
	\ifenglish\englishvocabulary\else\germanvocabulary\fi
	\ifcomputetimeused
	\else
		\renewcommand*{\scadsai@vocacknowledgement}{}
	\fi
	%
	% Setting size of the paper
	\ifanullb
	\ifportrait\special{papersize=91.4cm,129.3cm}\else\special{papersize=129.3cm,91.4cm}\fi
	\else\ifanull
	\ifportrait\special{papersize=83.96cm,118.82cm}\else\special{papersize=118.82cm,83.96cm}\fi
	\else\ifaeins
	\ifportrait\special{papersize=59.4cm,83.96cm}\else\special{papersize=83.96cm,59.4cm}\fi
	\else\ifazwei
	\ifportrait\special{papersize=41.98cm,59.4cm}\else\special{papersize=59.4cm,41.98cm}\fi
	\else\ifadrei
	\ifpreview
	\ifportrait\special{papersize=29.7cm,41.35cm}\else\special{papersize=41.98cm,29.0cm}\fi
	\else
	\ifportrait\special{papersize=29.7cm,41.98cm}\else\special{papersize=41.98cm,29.7cm}\fi
	\fi
	\else\relax
	\fi
	\fi
	\fi
	\fi
	\fi
	\makebackground\\\vspace{-3cm}\\%
	\foreach \i [count=\xi] in \@myfooterauthor{
		\gappto\mytablecontentsAuthorName{\setfooterauthorfont}
		\xappto\mytablecontentsAuthorName{\i}
		\ifnum\xi=\countItems{\@myfooterauthor}
		\else
			\gappto\mytablecontentsAuthorName{\tand}
		\fi
	}
	\foreach \j [count=\xj] in \@myfooteruni{
		\gappto\mytablecontentsAuthorUni{\color{tudblue}\setfooterunivfont}
		\xappto\mytablecontentsAuthorUni{\j}
		\ifnum\xj=\countItems{\@myfooteruni}
		\else
			\gappto\mytablecontentsAuthorUni{\tand}
		\fi
	}
	\foreach \k [count=\xk] in \@myfooteremail{
		\gappto\mytablecontentsAuthorEmail{\setfooterauthoremailfont}
		\xappto\mytablecontentsAuthorEmail{\k}
		\ifnum\xk=\countItems{\@myfooteremail}
		\else
			\gappto\mytablecontentsAuthorEmail{\tand}
		\fi
	}
	\maketitle%
}
%
\endinput
%% 
%% End of file `cls'.
