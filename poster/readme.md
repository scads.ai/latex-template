# ScaDS.AI Poster Template

## About

This is new ScaDS.AI poster template. The definitions of this new ScaDS.AI poster template can be found in [scadsai-poster-template.cls](scadsai-poster-template.cls). Sample compiled pdf can be found at [sample_poster](../other/sample_poster.pdf).

Please feel free to contribute to this project.
