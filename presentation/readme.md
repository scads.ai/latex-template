# ScaDS.AI Presentation Template

## About

This new ScaDS.AI presentation template is developed from an existing TU Dresden template (`tudbeamer.cls`). The old TU Dresden presentation can be found on the branch [tud-template](https://gitlab.hrz.tu-chemnitz.de/scads.ai/latex-template/-/tree/tud-template). The definitions of this new ScaDS.AI presentation template can be found in [scadsai-presentation-template.cls](scadsai-presentation-template.cls). Along with the comments present in `scadsai-presentation-template.cls`, [tudbeamer.pdf](../other/latex_reference.pdf) could be used as guide for implementing/changing designs, warning/code block. The rendered sample slide is present in [presentation sample](../other/sample_presentation.pdf) file.

Please feel free to contribute to this project.
